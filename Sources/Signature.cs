﻿using System;
using System.IO;
using System.Text;
using Iodynis.Libraries.Encryption;
using Iodynis.Libraries.Signing.Internal;

namespace Iodynis.Libraries.Signing
{
    public struct Signature
    {
        private const int SignatureBytesCount = 512;

        private const int PublicKeyBytesCountMinimum = SignatureBytesCount;
        private const int PublicKeyBytesCountMaximum = Int32.MaxValue;
        private const int PrivateKeyBytesCountMinimum = SignatureBytesCount;
        private const int PrivateKeyBytesCountMaximum = Int32.MaxValue;

        private const int PayloadBytesCountMinimum = 1;
        private const int PayloadBytesCountMaximum = Int32.MaxValue;

        private const int HashMd5BinaryLength = 16;
        public byte[] HashMd5Binary { get; }
        public string HashMd5Hex { get; }
        private const int HashSha1BinaryLength = 20;
        public byte[] HashSha1Binary { get; }
        public string HashSha1Hex { get; }
        private const int HashSha256BinaryLength = 32;
        public byte[] HashSha256Binary { get; }
        public string HashSha256Hex { get; }
        private readonly byte[] _Bytes;
        public byte[] Bytes
        {
            get
            {
                return (byte[])_Bytes.Clone();
            }
        }
        private Signature(byte[] bytes)
        {
            // Check
            if (bytes == null)
            {
                throw new Exception($"Can't create a signature: null provided but exactly {PayloadBytesCountMinimum} bytes of signature needed..");
            }
            if (bytes.Length != SignatureBytesCount)
            {
                throw new Exception($"Can't create a signature: {bytes.Length} bytes provided but exactly {SignatureBytesCount} bytes of signature needed.");
            }

            // Initialize
            byte[] md5 = bytes.Slice(bytes.Length - HashSha256BinaryLength - HashSha1BinaryLength - HashMd5BinaryLength, HashMd5BinaryLength);
            byte[] sha1 = bytes.Slice(bytes.Length - HashSha256BinaryLength - HashSha1BinaryLength, HashSha1BinaryLength);
            byte[] sha256 = bytes.Slice(bytes.Length - HashSha256BinaryLength, HashSha256BinaryLength);

            // Assign
            _Bytes = bytes;
            HashMd5Binary = md5;
            HashMd5Hex = Util.BytesToHex(HashMd5Binary);
            HashSha1Binary = sha1;
            HashSha1Hex = Util.BytesToHex(HashSha1Binary);
            HashSha256Binary = sha256;
            HashSha256Hex = Util.BytesToHex(HashSha256Binary);
        }
        private Signature(byte[] md5, byte[] sha1, byte[] sha256)
        {
            // Check
            if (md5 == null)
            {
                throw new Exception("MD5 hash can't be null.");
            }
            if (sha1 == null)
            {
                throw new Exception("SHA1 hash can't be null.");
            }
            if (sha256 == null)
            {
                throw new Exception("SHA256 hash can't be null.");
            }
            if (md5.Length != HashMd5BinaryLength)
            {
                throw new Exception($"MD5 hash should be exactly {HashMd5BinaryLength} bytes long.");
            }
            if (sha1.Length != HashSha1BinaryLength)
            {
                throw new Exception($"SHA1 hash should be exactly {HashSha1BinaryLength} bytes long.");
            }
            if (sha256.Length != HashSha256BinaryLength)
            {
                throw new Exception($"SHA256 hash should be exactly {HashSha256BinaryLength} bytes long.");
            }

            // Assign
            _Bytes = new byte[SignatureBytesCount];
            md5.CopyTo(_Bytes, _Bytes.Length - sha256.Length - sha1.Length - md5.Length);
            sha1.CopyTo(_Bytes, _Bytes.Length - sha256.Length - sha1.Length);
            sha256.CopyTo(_Bytes, _Bytes.Length - sha256.Length);
            HashMd5Binary = (byte[])md5.Clone();
            HashMd5Hex = Util.BytesToHex(HashMd5Binary);
            HashSha1Binary = (byte[])sha1.Clone();
            HashSha1Hex = Util.BytesToHex(HashSha1Binary);
            HashSha256Binary = (byte[])sha256.Clone();
            HashSha256Hex = Util.BytesToHex(HashSha256Binary);
        }
        public static Signature Read(string path, string publicKey)
        {
            if (publicKey == null)
            {
                throw new Exception("Can't read the signature: null provided but public key is needed.");
            }
            return Read(path, Encoding.ASCII.GetBytes(publicKey));
        }
        public static Signature Read(byte[] bytes, string publicKey)
        {
            if (publicKey == null)
            {
                throw new Exception("Can't read the signature: null provided but public key is needed.");
            }
            return Read(bytes, Encoding.ASCII.GetBytes(publicKey));
        }
        public static Signature Read(string path, byte[] publicKey)
        {
            if (path == null)
            {
                throw new Exception("Can't read the signature: null provided but file path is needed.");
            }
            if (!File.Exists(path))
            {
                throw new Exception($"Can't read the signature: file {path} does not exist.");
            }

            // Open file
            byte[] bytes;
            try
            {
                using (FileStream fileStream = File.OpenRead(path))
                {
                    // Check file length
                    if (fileStream.Length <= SignatureBytesCount + PayloadBytesCountMinimum /* At least 1 byte of data has to be presented */)
                    {
                        throw new EndOfStreamException($"File has the size of {fileStream.Length} bytes and is too small to have a signature, at least {SignatureBytesCount} bytes of signature and " +
                            $"{PayloadBytesCountMinimum} bytes of payload (that is {SignatureBytesCount + PayloadBytesCountMinimum} bytes total) needed.");
                    }
                    // Read all file
                    bytes = new byte[fileStream.Length];
                    fileStream.Position = fileStream.Length - SignatureBytesCount;
                    fileStream.Read(bytes, 0, bytes.Length);
                    fileStream.Close();
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Can't read the signature.", exception);
            }

            return Read(bytes, publicKey);
        }
        public static Signature Read(byte[] bytes, byte[] publicKey)
        {
            if (bytes == null)
            {
                throw new Exception($"Can't read the signature: null provided but at least {SignatureBytesCount} bytes of signature needed.");
            }
            if (bytes.Length < SignatureBytesCount)
            {
                throw new Exception($"Can't read the signature: {bytes.Length} bytes provided but at least {SignatureBytesCount} bytes of signature needed.");
            }
            if (publicKey == null)
            {
                throw new Exception("Can't check the signature: null provided but public key is needed.");
            }
            if (publicKey.Length < PublicKeyBytesCountMinimum)
            {
                throw new Exception($"Can't sign the payload: public key has {publicKey.Length} bytes but at least {PublicKeyBytesCountMinimum} is needed.");
            }
            if (publicKey.Length > PublicKeyBytesCountMaximum)
            {
                throw new Exception($"Can't read the signature: public key has {publicKey.Length} bytes but at most {PublicKeyBytesCountMaximum} is needed.");
            }

            byte[] signatureEncrypted = bytes.Slice(bytes.Length - SignatureBytesCount, SignatureBytesCount);
            byte[] signaturePlaintext = Decrypt(signatureEncrypted, publicKey);

            return new Signature(signaturePlaintext);
        }
        public static Signature Calculate(byte[] bytes)
        {
            if (bytes == null)
            {
                throw new Exception($"Can't calculate a signature: null provided but at least {PayloadBytesCountMinimum} bytes of payload needed.");
            }
            if (bytes.Length < PayloadBytesCountMinimum)
            {
                throw new Exception($"Can't calculate a signature: {bytes.Length} bytes of payload provided but at least {PayloadBytesCountMinimum} bytes of payload needed.");
            }
            if (bytes.Length > PayloadBytesCountMaximum)
            {
                throw new Exception($"Can't calculate a signature: {bytes.Length} bytes of payload provided but at most {PayloadBytesCountMaximum} bytes of payload needed.");
            }
            byte[] md5 = Util.BytesHashMd5(bytes); /* 16 bytes */
            byte[] sha1 = Util.BytesHashSha1(bytes); /* 20 bytes */
            byte[] sha256 = Util.BytesHashSha256(bytes); /* 32 bytes */

            return new Signature(md5, sha1, sha256);
        }
        public static bool Check(string path, byte[] publicKey)
        {
            if (path == null)
            {
                throw new Exception("Can't check the signature: null provided but file path is needed.");
            }
            if (!File.Exists(path))
            {
                throw new Exception($"Can't check the signature: file {path} does not exist.");
            }

            // Read file
            byte[] bytes;
            try
            {
                using (FileStream fileStream = File.OpenRead(path))
                {
                    // Check file length
                    if (fileStream.Length < SignatureBytesCount + PayloadBytesCountMinimum)
                    {
                        // Close file
                        fileStream.Close();
                        return false;
                    }
                    // Read all file
                    bytes = new byte[fileStream.Length];
                    fileStream.Position = 0;
                    fileStream.Read(bytes, 0, bytes.Length);
                    fileStream.Close();
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Can't check the signature.", exception);
            }

            // Check signature
            return Check(bytes, publicKey);
        }
        public static bool Check(byte[] bytes, byte[] publicKey)
        {
            if (bytes.Length <= SignatureBytesCount)
            {
                throw new Exception($"Can't check the signature: {bytes.Length} bytes of data provided but at least {SignatureBytesCount} bytes of signature and " +
                    $"{PayloadBytesCountMinimum} bytes of payload (that is {SignatureBytesCount + PayloadBytesCountMinimum} bytes total) needed.");
            }

            // Prepare buffers
            byte[] fileBytes = bytes.Slice(0, bytes.Length - SignatureBytesCount);
            byte[] signatureBytes = bytes.Slice(bytes.Length - SignatureBytesCount, SignatureBytesCount);

            // Get signatures
            Signature signatureRead = Read(signatureBytes, publicKey);
            if (signatureRead == null)
            {
                throw new Exception("Can't check the signature: can't read the signature.");
            }
            Signature signatureCalculated = Calculate(fileBytes);
            if (signatureCalculated == null)
            {
                throw new Exception("Can't check the signature: can't calculate the signature.");
            }

            return signatureRead == signatureCalculated;
        }
        public static Signature Sign(string path, string publicKey)
        {
            if (publicKey == null)
            {
                throw new Exception("Can't sign the payload: null provided but public key is needed.");
            }
            return Sign(path, Encoding.ASCII.GetBytes(publicKey));
        }
        public static Signature Sign(string path, byte[] keyPrivate)
        {
            if (path == null)
            {
                throw new Exception("Can't sign the payload: null provided but file path is needed.");
            }
            if (!File.Exists(path))
            {
                throw new Exception($"Can't sign the payload: file {path} does not exist.");
            }
            if (keyPrivate.Length < PrivateKeyBytesCountMinimum)
            {
                throw new Exception($"Can't sign the payload: private key has {keyPrivate.Length} bytes but at least {PrivateKeyBytesCountMinimum} is needed.");
            }
            if (keyPrivate.Length > PrivateKeyBytesCountMaximum)
            {
                throw new Exception($"Can't sign the payload: private key has {keyPrivate.Length} bytes but at most {PrivateKeyBytesCountMaximum} is needed.");
            }

            // Read file
            byte[] fileBytes = null;
			try
			{
				using (FileStream fileStreamReader = File.OpenRead(path))
				{
					fileBytes = new byte[fileStreamReader.Length];
					fileStreamReader.Position = 0;
					fileStreamReader.Read(fileBytes, 0, fileBytes.Length);
				}
            }
			catch (Exception exception)
			{
				throw new Exception($"Can't sign the payload: failed to read file {path}.", exception);
			}
            if (fileBytes.Length < PayloadBytesCountMinimum)
            {
                throw new Exception($"Can't sign the payload: File + {path} has size of {fileBytes.Length} bytes but at least {PayloadBytesCountMinimum} is needed.");
            }
            if (fileBytes.Length > PayloadBytesCountMaximum)
            {
                throw new Exception($"Can't sign the payload: File + {path} has size of {fileBytes.Length} bytes but at most {PayloadBytesCountMaximum} is needed.");
            }

            // Get encrypted signature to append to the file
            Signature signature = Calculate(fileBytes);
            byte[] signatureBytesPlaintext = signature.Bytes;
            byte[] signatureBytesEncrypted = Encrypt(signatureBytesPlaintext, keyPrivate);

            // Write signature
            using (FileStream fileStreamWriter = new FileStream(path, FileMode.Append))
            {
                fileStreamWriter.Position = fileStreamWriter.Length;
                fileStreamWriter.Write(signatureBytesEncrypted, 0, signatureBytesEncrypted.Length);
                fileStreamWriter.Close();
            }

            return signature;
        }
        private static byte[] Encrypt(byte[] bytes, byte[] privateKey)
        {
            return RSA.Decrypt(bytes, privateKey);
        }
        private static byte[] Decrypt(byte[] bytes, byte[] publicKey)
        {
            return RSA.Encrypt(bytes, publicKey);
        }
        public static bool operator == (Signature signatureOne, Signature signatureTwo)
        {
            return signatureOne.HashMd5Hex == signatureTwo.HashMd5Hex && signatureOne.HashSha1Hex == signatureTwo.HashSha1Hex && signatureOne.HashSha256Hex == signatureTwo.HashSha256Hex;
        }
        public static bool operator != (Signature signatureOne, Signature signatureTwo)
        {
            return !(signatureOne == signatureTwo);
        }
        public override bool Equals(object signature)
        {
            return signature is Signature && (Signature)signature == this;
        }
        public override int GetHashCode()
        {
            return HashMd5Hex.GetHashCode() ^ HashSha1Hex.GetHashCode() ^ HashSha256Hex.GetHashCode();
        }
        public override string ToString()
        {
            return HashMd5Hex + HashSha1Hex + HashSha256Hex;
        }
    }
}
