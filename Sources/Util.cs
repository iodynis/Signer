﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Signing.Internal
{
    public static class Util
    {
        public static T[] Slice<T>(this T[] array, int index, int length)
        {
            if (index < 0)
            {
                throw new ArgumentException("Index cannot be negative.", nameof(index));
            }
            if (index >= array.Length)
            {
                throw new IndexOutOfRangeException($"Start index {index} is out of the range of the array. Array size is {array.Length}.");
            }
            if (length < 0)
            {
                throw new ArgumentException("Length cannot be negative.", nameof(length));
            }
            if (index + length >= array.Length)
            {
                throw new IndexOutOfRangeException($"End index {index + length} is out of the range of the array. Array size is {array.Length}.");
            }

            T[] arraySliced = new T[length];
            Array.Copy(array, index, arraySliced, 0, length);
            return arraySliced;
        }
        public static string BytesToHex(byte[] bytes)
        {
	        if (bytes == null)
	        {
		        throw new ArgumentNullException(nameof(bytes), "Array cannot be null.");
	        }
            // Convert the byte array to hexadecimal string
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                stringBuilder.Append(bytes[i].ToString("X2"));
            }
            return stringBuilder.ToString();
        }
        public static byte[] BytesHashMd5(byte[] bytes)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] hash = md5.ComputeHash(bytes);
                return hash;
            }
        }
        public static byte[] BytesHashSha1(byte[] bytes)
        {
            using (System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1.Create())
            {
                byte[] hash = sha1.ComputeHash(bytes);
                return hash;
            }
        }
        public static byte[] BytesHashSha256(byte[] bytes)
        {
            using (System.Security.Cryptography.SHA256 sha256 = System.Security.Cryptography.SHA256.Create())
            {
                byte[] hash = sha256.ComputeHash(bytes);
                return hash;
            }
        }
    }
}
